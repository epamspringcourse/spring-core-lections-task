package com.epam.spring.loggers;

import com.epam.spring.events.Event;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Ilia Efimov on 23.05.2017.
 */
public class FileEventLogger implements EventLogger {
    private String fileName;
    private File file;

    public FileEventLogger(String pFileName) {
        this.fileName = pFileName;
    }

    public void init() throws IOException {
        this.file = new File(getFileName());
        if (!this.file.canWrite()) {
            throw new IOException("Permission denied!");
        }
    }

    public void logEvent(Event pEvent) {
        writeFile(pEvent.toString());
    }

    protected void writeFile(String pString) {
        try {
            if (file == null) {
                System.out.println("File not found");
            }
            FileUtils.writeStringToFile(file, String.format("%s\n", pString), "UTF-8", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
