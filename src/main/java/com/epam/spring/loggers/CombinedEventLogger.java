package com.epam.spring.loggers;

import com.epam.spring.events.Event;
import java.util.List;

/**
 * Created by Ilia Efimov on 28.05.2017.
 */
public class CombinedEventLogger implements EventLogger {
    List<EventLogger> loggers;

    public CombinedEventLogger(final List<EventLogger> pLoggers) {
        this.loggers = pLoggers;
    }

    public void logEvent(Event pEvent) {
        if (loggers == null) {
            throw new NullPointerException();
        }
        for (EventLogger logger : loggers) {
            logger.logEvent(pEvent);
        }
    }
}
