package com.epam.spring.loggers;

import com.epam.spring.events.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilia Efimov on 23.05.2017.
 */
public class CacheFileLogger extends FileEventLogger {
    private static final byte DEFAULT_CACHE_SIZE = 10;
    private List<Event> cache = new ArrayList<Event>(DEFAULT_CACHE_SIZE);

    public CacheFileLogger(String pFileName) {
        super(pFileName);
    }

    public void destroy() {
        if (!cache.isEmpty()) {
            flushCache();
        }
    }

    public void logEvent(Event pEvent) {
        if (cache.size() < DEFAULT_CACHE_SIZE) {
            cache.add(pEvent);
        } else {
            flushCache();
            cache.add(pEvent);
        }
    }

    private void flushCache() {
        for (Event event : cache) {
            writeFile(event.toString());
        }
        cache.clear();
    }
}
