package com.epam.spring.loggers;

import com.epam.spring.events.Event;

/**
 * Created by Ilia Efimov on 21.05.2017.
 */
public interface EventLogger {
    public void logEvent(Event pEvent);
}
