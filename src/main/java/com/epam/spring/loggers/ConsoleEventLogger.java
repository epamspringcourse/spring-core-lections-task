package com.epam.spring.loggers;

import com.epam.spring.events.Event;

/**
 * Created by Ilia Efimov on 21.05.2017.
 */
public class ConsoleEventLogger implements EventLogger {
    public void logEvent(Event pEvent) {
        System.out.println(pEvent);
    }
}
