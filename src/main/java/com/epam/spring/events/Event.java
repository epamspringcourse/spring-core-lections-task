package com.epam.spring.events;

import org.springframework.cglib.core.Local;

import java.text.DateFormat;
import java.time.LocalTime;
import java.util.Date;

/**
 * Created by Ilia Efimov on 21.05.2017.
 */
public class Event {
    private int id;
    private String msg;
    private Date date;
    private DateFormat dateFormat;

    public Event(Date pDate, DateFormat pDateFormat) {
        this.setDate(pDate);
        this.setDateFormat(pDateFormat);
    }

    public DateFormat getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    public int getId() {
        return id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public static boolean isDay() {
        LocalTime timeFirst = LocalTime.parse("08:00");
        LocalTime timeLast = LocalTime.parse("17:00");
        LocalTime currentTime = LocalTime.now();
        return currentTime.isAfter(timeFirst) && currentTime.isBefore(timeLast);
    }

    @Override
    public String toString() {
        return String.format("%d %s: %s", id, dateFormat.format(date), msg);
    }
}
