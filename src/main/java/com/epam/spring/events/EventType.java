package com.epam.spring.events;

/**
 * Created by Ilia Efimov on 28.05.2017.
 */
public enum EventType {
    INFO,
    ERROR;
}
