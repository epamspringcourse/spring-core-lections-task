package com.epam.spring;

import com.epam.spring.beans.Client;
import com.epam.spring.events.Event;
import com.epam.spring.events.EventType;
import com.epam.spring.loggers.EventLogger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

public class App 
{
    private Client client;
    private EventLogger defaultLogger;
    private Map<EventType, EventLogger> eventLoggers;

    private static final ConfigurableApplicationContext ctx =
            new ClassPathXmlApplicationContext("spring.xml");

    public App(final Client pClient, final EventLogger pDefaultLogger,
               final Map<EventType, EventLogger> pEventLoggers) {
        this.client = pClient;
        this.eventLoggers = pEventLoggers;
        this.defaultLogger = pDefaultLogger;
    }

    public static void main( String[] args ) {
        App app = (App) ctx.getBean("app");
        app.logEvent("INFO Event Some event for User 1", EventType.INFO);
        app.logEvent("ERROR Event Some event for User 1", EventType.ERROR);
        app.logEvent("Default Event Some event for User 1", null);
        ctx.close();
    }

    private void logEvent(String logMessage, final EventType eventType) {
        String logMsg = logMessage.replaceAll(client.getId(), client.getFullName());
        Event event = (Event) ctx.getBean("event");
        event.setMsg(logMsg);
        EventLogger eventLogger = eventLoggers.get(eventType);
        if (eventLogger == null) {
            eventLogger = this.defaultLogger;
        }
        eventLogger.logEvent(event);
    }
}
