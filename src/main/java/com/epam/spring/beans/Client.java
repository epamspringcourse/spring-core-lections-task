package com.epam.spring.beans;

/**
 * Created by Ilia Efimov on 21.05.2017.
 */
public class Client {
    private String id;
    private String fullName;
    private String greeting;

    public Client(String pId, String pName) {
        this.setId(pId);
        this.setFullName(pName);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }
}
